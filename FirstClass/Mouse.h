//
//  Mouse.h
//  FirstClass
//
//  Created by hh on 16/4/6.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Animal.h"

@interface Mouse : Animal

- (void)makeRole;

@end
