//
//  Restorant.m
//  FirstClass
//
//  Created by hh on 16/4/13.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Restorant.h"

@implementation Restorant

- (void)pay:(float)money
{
    NSLog(@"收到了 ¥%f ", money);
}

- (void)getFood:(NSString *)str
{
    NSLog(@"顾客点了菜： %@", str);
}

- (void)otherMethod
{
    NSLog(@"我们还有很多别的菜！");
}

@end
