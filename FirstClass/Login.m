//
//  Login.m
//  FirstClass
//
//  Created by hh on 16/4/14.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Login.h"

@implementation Login
{
    NSString *name;
    NSString *passwd;
}

- (void)getName
{
    NSLog(@"请输入用户名：");
    char* name_ = malloc(20);
    scanf("%s", name_);
    name = [[NSString alloc] initWithCString:name_ encoding:NSUTF8StringEncoding];
    [self getPasswd];
}
- (void)getPasswd
{
    NSLog(@"请输入密码：");
    char* pass = malloc(20);
    scanf("%s", pass);
    passwd = [[NSString alloc] initWithCString:pass encoding:NSUTF8StringEncoding];
    if ([name isEqualToString:@"test"] && [passwd isEqualToString:@"123"]) {
        NSLog(@"登录成功！");
        [self.delegate loginWithName:name passwd:passwd success:YES];
    }else{
        NSLog(@"登录失败！");
        [self.delegate loginWithName:name passwd:passwd success:NO];
    }
}

@end
