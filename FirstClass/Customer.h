//
//  Customer.h
//  FirstClass
//
//  Created by hh on 16/4/13.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EatDelegate <NSObject>

@required
- (void)pay:(float)money;
- (void)getFood:(NSString *)str;

@optional
- (void)otherMethod;

@end

@interface Customer : NSObject

@property (weak,nonatomic) id<EatDelegate> delegate;

- (void)comeToRestorant;

@end
