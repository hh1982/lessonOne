//
//  Customer.m
//  FirstClass
//
//  Created by hh on 16/4/13.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Customer.h"

@implementation Customer

- (void)comeToRestorant
{
    [self.delegate getFood:@"糖醋里脊"];
    [self.delegate pay:99.0];
    
    if ([self.delegate respondsToSelector:@selector(otherMethod)]) {
        [self.delegate otherMethod];
    }
}

@end
