//
//  Login.h
//  FirstClass
//
//  Created by hh on 16/4/14.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoginDelegate <NSObject>

- (void)loginWithName:(NSString *)name passwd:(NSString *)passwd success:(BOOL)ok;

@end

@interface Login : NSObject

@property (weak,nonatomic) id<LoginDelegate> delegate;

- (void)getName;

@end
