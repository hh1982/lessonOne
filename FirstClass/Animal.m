//
//  Animal.m
//  FirstClass
//
//  Created by hh on 16/4/5.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Animal.h"

@implementation Animal

- (NSString *)description
{
    return self.name;
}

- (void)eat
{
    NSLog(@"Animal eat!");
}

- (void)sleep
{
    NSLog(@"Animal sleep!");
}

- (NSComparisonResult)compare:(Animal *)ani
{
    return [self.name compare:ani.name];
}

@end
