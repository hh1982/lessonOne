//
//  Mouse.m
//  FirstClass
//
//  Created by hh on 16/4/6.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Mouse.h"

@implementation Mouse

- (void)eat
{
    NSLog(@"Mouse %@ eats! ", self.name);
}

- (void)sleep
{
    NSLog(@"Mouse %@ sleep at day time!",self.name);
}

- (void)makeRole
{
    NSLog(@"Mouse makeRole!");
}

@end
