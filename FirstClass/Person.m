//
//  Person.m
//  FirstClass
//
//  Created by hh on 16/4/5.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Person.h"

@implementation Person

- (void)eat
{
    NSLog(@"Person %@ eats!", self.name);
}

- (void)sleep
{
    NSLog(@"Person %@ sleep!",self.name);
}

- (void)talk:(NSString *)text
{
    NSLog(@"Person %@ says: %@!", self.name, text);
}

@end
