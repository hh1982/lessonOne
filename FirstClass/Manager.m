//
//  Manager.m
//  FirstClass
//
//  Created by hh on 16/4/14.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Manager.h"

@implementation Manager

- (void)loginWithName:(NSString *)name passwd:(NSString *)passwd success:(BOOL)ok
{
    NSLog(@"管理者：用户%@登录%@", name, ok?@"成功":@"失败");
}

@end
