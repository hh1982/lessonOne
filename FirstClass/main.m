//
//  main.m
//  FirstClass
//
//  Created by hh on 16/4/5.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Atom.h"
#import "Person.h"
#import "Mouse.h"
#import "Restorant.h"
#import "Manager.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        Animal *ani = [[Animal alloc] init];
        ani.name = @"我是动物类";
        
        Person *p = [[Person alloc] init];
        p.name = @"陈泽";
        
        Mouse *m = [[Mouse alloc] init];
        m.name = @"米老鼠";
        
        NSArray *arr = @[ani, p, m];
        NSLog(@"names: %@", [arr componentsJoinedByString:@","]);
        
        NSString *str = @"a,d,c,b";
        NSArray *ano = [str componentsSeparatedByString:@","];
        ano = [ano sortedArrayUsingSelector:@selector(compare:)];
        for (int i = 0; i < ano.count; i++) {
            NSString *s = ano[i];
            NSLog(@"%d、%@", i, s);
        }
        
        NSArray *numbers = @[@(1), @(4), @(3), @(2)];
        numbers = [numbers sortedArrayUsingSelector:@selector(compare:)];
        for (int i = 0; i < numbers.count; i++) {
            NSNumber *s = numbers[i];
            NSLog(@"%d、%@", i, s);
        }
        
        Restorant *resto = [[Restorant alloc] init];
        Customer *cust = [[Customer alloc] init];
        cust.delegate = resto;
        [cust comeToRestorant];
        
        Login *log = [[Login alloc] init];
        Manager *man = [[Manager alloc] init];
        log.delegate = man;
        [log getName];
    }
    return 0;
}
