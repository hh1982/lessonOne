//
//  Atom.h
//  FirstClass
//
//  Created by hh on 16/4/5.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Atom : NSObject

@property (readonly) NSUInteger protons;
@property (readonly) NSUInteger neutrons;
@property (readonly) NSUInteger electrons;
@property (readonly) NSString *chemicalElement;

- (NSUInteger) massNumber;

//- (NSUInteger)protons;
//- (void)setProtons:(NSUInteger)protons;

@end
