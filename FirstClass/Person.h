//
//  Person.h
//  FirstClass
//
//  Created by hh on 16/4/5.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "Animal.h"

@interface Person : Animal

- (void)talk:(NSString *)text;

@end
