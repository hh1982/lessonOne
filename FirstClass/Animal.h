//
//  Animal.h
//  FirstClass
//
//  Created by hh on 16/4/5.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject

@property (strong,nonatomic) NSString *name;

- (void)eat;
- (void)sleep;

- (NSComparisonResult)compare:(Animal *)ani;

@end
